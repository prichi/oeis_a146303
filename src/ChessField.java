import java.util.ArrayList;
import java.util.List;

/**
 * @author richi
 *
 */
public class ChessField {

	public static String QUEEN = "Q";
	public static String THREATENED = "T";
	
	/**
	 * array of the chess board
	 */
	private String [][] field;

	/**
	 * length/width of the chess board
	 */
	private final int n; 
	
	/**
	 * string list representing the results
	 */
	private List<String> allResults;

	/**
	 * constructor of the {@link ChessField} class
	 * @param n length/width of the chess board
	 */
	public ChessField(int n) {
		this.n = n;
		this.field = new String[this.n][this.n];
		this.resetChessBoard();
		this.allResults = new ArrayList<String>();
	}
	
	/**
	 * constructor of the {@link ChessField} class
	 * @param setField String array to use for setting the chess field
	 */
	public ChessField(String[][] setField) {
		this.n = setField.length;
		this.field = new String[this.n][this.n];
		this.allResults = new ArrayList<String>();

		for(int i = 0; i < this.n; i++) {
			for(int j = 0; j < this.n; j++) {
				this.getField()[i][j] = setField[i][j];
			}
		}
	}

	/**
	 * setting all chess board fields to ""
	 */
	private void resetChessBoard() {
		for(int i = 0; i < this.n; i++) {
			for(int j = 0; j < this.n; j++) {
				this.getField()[i][j] = "";
			}
		}
	}

	/**
	 * starting calculating the results, 
	 * by starting with the first queen in each field of the board,
	 * after each other by using the recursive method {@link ChessField#start(int, int)}
	 * for each Chess board index
	 * Adding all results which are not already part of the result list in {@link ChessField#allResults}
	 */
	public void calculateQueens() {
		for(int i = 0; i < this.n; i++) {
			for(int j = 0; j < this.n; j++) {
				List<String> resultList = this.start(i, j);
				for (String currentResult : resultList) {
					if(!this.getAllResults().contains(currentResult)) {
						this.getAllResults().add(currentResult);
					}
				}
				this.resetChessBoard();
			}
		}
	}

	public List<String> start(int queenRow, int queenColumn) {
		this.setQueen(queenRow, queenColumn);
		List<String> resultList = new ArrayList<String>();
		List<FreeField> freeFields = this.getFreeFields();
		if(freeFields.size() > 0) {
			for (FreeField freeField : freeFields) {
				ChessField newChessField = new ChessField(this.getField());
				List<String> newChessFieldResultList = newChessField.start(freeField.getI(), freeField.getJ());
				for (String currentResult : newChessFieldResultList) {
					if(!resultList.contains(currentResult)) {
						resultList.add(currentResult);
					}
				}
			}
		} else {
			resultList.add(this.toString());
		}
		return resultList;
	}
	
	/**
	 * return a list of all free fields of the chess board
	 * @return list of class FeeField representing all remaining unset fields of the chess board
	 */
	private List<FreeField> getFreeFields() {
		List<FreeField> freeFieldList = new ArrayList<FreeField>();
		
		for(int i = 0; i < this.n; i++) {
			for(int j = 0; j < this.n; j++) {
				if("".equals(this.field[i][j])) {
					freeFieldList.add(new FreeField(i, j));
				}
			}
		}
		return freeFieldList;
	}

	/**
	 * set queen {@link ChessField#QUEEN} to field i, j and 
	 * set the threatened fields to {@link ChessField#THREATENED}
	 * @param i row to set the queen
	 * @param j column to set the queen
	 */
	public void setQueen(int i, int j) {
		this.setField(i, j, ChessField.QUEEN);

		// set row threatened
		for(int currentRow = 0; currentRow < this.n; currentRow++) {
			this.setField(currentRow, j, ChessField.THREATENED);
		}
		
		// set colum threatened
		for(int currentColumn = 0; currentColumn < this.n; currentColumn++) {
			this.setField(i, currentColumn, ChessField.THREATENED);
		}
		
		/*
		 * Diagonale links oben bis rechts unten
		 */
		int currentRow = i - 1;
		int currentColumn = j - 1;
		while(currentRow >= 0 && currentColumn >= 0) {
			this.setField(currentRow, currentColumn, ChessField.THREATENED);
			currentRow--;
			currentColumn--;
		}
		
		currentRow = i+1;
		currentColumn = j+1;
		while(currentRow < this.n && currentColumn < this.n) {
			this.setField(currentRow, currentColumn, ChessField.THREATENED);
			currentRow++;
			currentColumn++;
		}
		
		/*
		 * Diagonale links unten bis rechts oben
		 */
		currentRow = i-1;
		currentColumn = j+1;
		while(currentRow >= 0 && currentColumn < this.n) {
			this.setField(currentRow, currentColumn, ChessField.THREATENED);
			currentRow--;
			currentColumn++;
		}
		
		currentRow = i+1;
		currentColumn = j-1;
		while(currentRow < this.n && currentColumn >= 0) {
			this.setField(currentRow, currentColumn, ChessField.THREATENED);
			currentRow++;
			currentColumn--;
		}
	}
	
	/**
	 * @return the field
	 */
	public String[][] getField() {
		return field;
	}

	/**
	 * @param field the field to set
	 */
	public boolean setField(int i, int j, String value) {
		if("".equals(this.field[i][j])) {
			this.field[i][j] = value;
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @return the n
	 */
	public int getN() {
		return n;
	}
	
	/**
	 * returns all results of the current calcuation
	 * @return
	 */
	public List<String> getAllResults() {
		return this.allResults;
	}

	/**
	 * returns the String represnetation of the Chess Board
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		for(int i = 0; i < this.n; i++) {
			for(int j = 0; j < this.n; j++) {
				if("".equals(this.field[i][j])) {
					sb.append(" ");
					sb.append(",");
				} else {
					sb.append(this.field[i][j]);
					sb.append(",");
				}
			}
			sb.append("\n");
		}
		return sb.toString();
	}
}
