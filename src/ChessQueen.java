import java.util.Collections;
import java.util.List;

/**
 * 
 * @author richi
 *
 */
public class ChessQueen {

	/**
	 * main to calculate the Chess Field
	 * @param args
	 */
	public static void main(String[] args) {
		ChessField cf = new ChessField(6);
		cf.calculateQueens();
		
		List<String> resultList = cf.getAllResults();
		System.out.println("Different Results: " + resultList.size());
		System.out.println("------------------------");
		for (String string : resultList) {
			System.out.println(string);
			System.out.println("------------------------");
		}
	}
}
