import java.lang.reflect.Constructor;

/**
 * 
 */

/**
 * @author richi
 *
 */
public class FreeField {

	private final int i; 
	private final int j;
	
	/**
	 * {@link Constructor}
	 */
	public FreeField(int i, int j) {
		this.i = i;
		this.j = j;
	}

	/**
	 * @return the i
	 */
	public int getI() {
		return i;
	}

	/**
	 * @return the j
	 */
	public int getJ() {
		return j;
	}
}
